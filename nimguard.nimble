# Package

version       = "0.1.0"
author        = "Tim Roberts"
description   = "Guards one or more files and folders and run command on change"
license       = "MIT"
srcDir        = "src"
bin           = @["nimguard"]


# Dependencies

requires "nim >= 1.4.2", "argparse", "glob"
