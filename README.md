# Nimguard
Nimguard is a program that watches on or more folder and executes a specified
command when any file is modified

Can be used like a repl for compiled language by continously rebuilding and
restarting the program when saving a file

files and directories in .gitignore or .ignore are automatically skipped unless
--no-ignore is passed

## Installation
Requires the nim toolchain. Install nim at [https://nim-lang.org/install.html](https://nim-lang.org/install.html)

```
git clone https://gitlab.com/ten3roberts/nimguard
cd nimguard
nimble install
```

## Usage
Use `nimguard --help` for a list of commands

Automatically run `make` when a file in `src` changes
```
nimguard src -x "make"
```

Automatically run `make` and `program` when a file in `src` changes
Kill program if running when file changes again. Useful to restart a program
when developing
```
nimguard src -kx "make && ./program"
```
