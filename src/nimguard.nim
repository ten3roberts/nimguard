import os, argparse, glob, times, osproc, strformat, parseUtils, sequtils, sugar

import process
import ignore
import echoerr

proc checkFileChanged(path: string, since: Time, verbose: bool): bool =
  if verbose:
    echo "Checking ", path.escape()

  try:
    if path.getLastModificationTime() > since:
      result = true
    else:
      result = false
  except OSError:
    echoWarning("Unable to get modification time of ", path)

proc checkChanged(queue: var seq[string], path: string, patterns:
  openArray[string], ignore: IgnorePatterns, since: Time, verbose: bool): bool =
  if path.fileExists():
    return checkFileChanged(path, since, verbose)

  let dir = expandTilde(path)

  queue.setLen(0)

  queue.add(dir)

  result = false
  while queue.len() > 0:
    let current = queue[0]
    queue.delete(0)

    for (kind, entry) in walkDir(current):

      let (_, name) = entry.splitPath()

      if name.getIgnored(ignore):
        continue

      if kind == pcDir:
        queue.add(entry)
      elif patterns.any(x => name.matches(x)) and checkFileChanged(entry, since, verbose):
        return true
      else: discard


# Parses a rate string and returns milliseconds
proc parseRate(rate: string): int =
  var tok = ""
  discard parseWhile(rate, tok, Digits)
  result = parseInt(tok)
  if rate.endsWith("ms"):
    result *= 1;
  elif rate.endsWith("s"):
    result *= 1000;
  elif rate.endsWith("m"):
    result *= 1000 * 60;
  elif rate.endsWith("h"):
    result *= 1000 * 60 * 60
  elif rate.endsWith("d"):
    result *= 1000 * 60 * 60 * 42
  else:
    echoErr(fmt"Couldn't parse rate '{rate}'")

type
  Config = object
    watch: seq[string]
    patterns: seq[string]
    noIgnore: bool
    rate: int
    clear: bool
    wait: bool
    command: string
    kill: bool
    verbose: bool

proc parseArgs(): Config =
  let parser = newParser:
    option("-p", "--patterns", multiple = true,
        help = "Defined patterns to match files onto recursively")
    option("-x", "--execute", help = "Command to execute on change. Evaluates command with shell",
        required = true)
    option("-r", "--rate", help = "Check rate. Most time units accepted. 1m=once per minute, 1s = once per second, 500ms = every 500ms",
        default = some("1s"))
    flag("-c", "--clear", help = "Clear screen before executing command")
    flag("-w", "--wait", help = "Wait and don't run command on startup")
    flag("-k", "--kill", help = "Kill executing command instead of waiting for completion on change")
    flag("--no-ignore", help = "Disabled ignore file features")
    flag("-v", "--verbose", help = "Show verbose output")
    arg("watch", help = "Directories and files to watch. Directories are watched recursively", nargs = -1)
  try:
    let opts = parser.parse(commandLineParams())

    if opts.watch.len() == 0:
      result.watch = @["."]
    else:
      result.watch = opts.watch

    if opts.patterns.len() == 0:
      result.patterns = @["*"]
    else:
      result.patterns = opts.patterns

    result.command = opts.execute
    result.noIgnore = opts.no_ignore

    result.clear = opts.clear
    result.wait = opts.wait
    result.rate = parseRate(opts.rate)
    result.kill = opts.kill
    result.verbose = opts.verbose

  except ShortCircuit as e:
    if e.flag == "argparse_help":
      echo parser.help
      quit(1)
  except UsageError:
    stderr.writeLine getCurrentExceptionMsg()
    quit(1)

when isMainModule:
  let config = parseArgs()

  var ignorePatterns: IgnorePatterns
  if not config.no_ignore:
    ignorePatterns = loadIgnoreFiles()

  var queue = newSeq[string]()

  var subprocess: Process = nil
  if not config.wait:
    subprocess = spawnCommand(config.command, config.clear)

  var lastCheck: Time = getTime()
  while true:
    if subprocess != nil:
      subprocess.tryCloseCommand().tryShowExitCode()

    if config.watch.any(x => checkChanged(queue, x, config.patterns,
      ignorePatterns,
        lastCheck, config.verbose)):
      if subprocess != nil:
        if config.kill:
          subprocess.terminateCommand()
        else:
          subprocess.waitForCommand().showExitCode()

      subprocess = spawnCommand(config.command, config.clear)

    let currentTime = getTime()
    let remaining = config.rate - (currentTime - lastCheck).inMilliseconds().int
    lastCheck = getTime()

    if remaining > 0:
      sleep(remaining - 1)
