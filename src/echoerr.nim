import terminal

proc echoErr*(args: varargs[string, `$`]) =
  setforegroundcolor(stdout, fgred)
  setstyle(stdout, { stylebright })
  stdout.write(" Error: ");
  resetattributes(stdout)

  for x in args:
    stdout.write(x)

  stdout.write("\n")


proc echoWarning*(args: varargs[string, `$`]) =
  setforegroundcolor(stdout, fgYellow)
  setstyle(stdout, { stylebright })
  stdout.write(" Warning: ");
  resetattributes(stdout)

  for x in args:
    stdout.write(x)

  stdout.write("\n")
