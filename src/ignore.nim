import glob, os, strutils
type
  IgnorePatterns* = seq[string]


proc loadIgnore*(path: string): IgnorePatterns =
  var file = open(path, fmRead)

  for line in file.lines():
    result.add(line)


# Returns true if file is ignored
proc getIgnored*(path: string, patterns: IgnorePatterns): bool =
  var str = path

  # Remove './'
  if str.startsWith("./"):
    str = str.substr(2)

  for pattern in patterns:
    if str.matches(pattern):
      return true;

  return false

proc tryLoadIgnore(path: string): IgnorePatterns =
  if not fileExists(path):
    return @[]

  echo "Found ", path
  result = loadIgnore(path)

proc loadIgnoreFiles*(): IgnorePatterns =
  let gitignore = tryLoadIgnore(".gitignore")
  if gitignore.len() > 0:
    result.add gitignore
    result.add ".git"
  result.add tryLoadIgnore(".ignore")
