import osproc, strutils, strformat, options, terminal
import echoerr

proc echoColor(fg: ForegroundColor, style: set[Style], msg: varargs[string, `$`]) =
  setForegroundColor(stdout, fg)
  setStyle(stdout, style)

  for x in msg:
    stdout.write(x)

  resetAttributes(stdout)
  stdout.write("\n")

## Spawns the specified command and returns the process
proc spawnCommand*(command: string, clear: bool): Process {.raises: [Exception].} =
  if clear: eraseScreen(stdout)

  echoColor(fgCyan, { styleBright }, fmt"Running {command.escape}")
  try:
    result = startProcess(command, options={poEvalCommand, poParentStreams})
  except OSError:
    echoErr "Failed to execute command. ", getCurrentExceptionMsg().escape

## Waits for the command to finish executing
## Sets process to nil
proc waitForCommand*(process: var Process): int =
  echoColor(fgMagenta, { styleBright }, "Waiting...")
  result = process.waitForExit()
  # If killed using sigint, it may be nil
  if process != nil:
    process.close()
    process = nil

## Closes the process if it has finished and returns the exit code. Sets process to nil
## Does nothing if running
proc tryCloseCommand*(process: var Process): Option[int] {.raises: [OSError, Exception].} =
  if process.running():
    result = none(int)
  else:
    result = some(process.waitForExit())
    process.close()
    process = nil

## Sends sigterm running process
proc terminateCommand*(process: var Process) {.raises: [Exception].} =
  echoColor(fgMagenta, { styleBright }, "Terminating command")

  terminate(process)
  process.close()
  process = nil

## Shows exit code to the user
proc showExitCode*(exitCode: int) =
  var color = if exitCode == 0: fgGreen else: fgRed
  echoColor(color, { styleBright }, "Finished with code ", exitCode)

## Shows exit code if some
proc tryShowExitCode*(exitCode: Option[int]) =
  if exitCode.isSome(): exitCode.get().showExitCode()
